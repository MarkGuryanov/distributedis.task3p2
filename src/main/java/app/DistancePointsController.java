package app;

import app.entity.NodesEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class DistancePointsController {
    private final NodesRepository repository;

    @Autowired
    public DistancePointsController(NodesRepository repository) {
        this.repository = repository;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/inRadiusFrom")
    public List<NodesEntity> pointsInRadiusFrom(
            @RequestParam("lat") double lat,
            @RequestParam("lon") double lon,
            @RequestParam("rad") double rad) {
        return repository.findInRadiusFrom(lat, lon, rad);
    }
}
