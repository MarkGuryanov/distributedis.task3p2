package app;

import app.entity.NodesEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NodesRepository extends JpaRepository<NodesEntity, Long> {
    @Query(value = "SELECT * FROM nodes " +
            "WHERE (point(lat, lon) <@> point(:lat, :lon)) * 1609.34 < :rad " +
            "ORDER BY point(lat, lon) <@> point(:lat, :lon) ASC", nativeQuery = true)
    List<NodesEntity> findInRadiusFrom(@Param("lat") double lat, @Param("lon") double lon, @Param("rad") double rad);
}
