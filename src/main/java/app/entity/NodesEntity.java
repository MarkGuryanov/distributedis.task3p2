package app.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

@Entity @Table(name = "nodes")
@Getter @Setter @EqualsAndHashCode
public class NodesEntity {
    @Id private long id;
    @Basic private Double lat;
    @Basic private Double lon;
    @Basic private String username;
    @Basic private Long uid;
    @Basic private Boolean visible;
    @Basic private Long version;
    @Basic private Long changeset;
    @Basic private Timestamp timestamp;

    @Convert(converter = app.entity.MapToHStoreConverter.class)
    @Column(columnDefinition = "hstore")
    private Map<String, String> tags = new HashMap<>();
}
